// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
livereload = require('gulp-livereload');

// Lint Task
gulp.task('lint', function() {
    return gulp.src('client/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Compile Our Sass
gulp.task('sass', function() {
    return gulp.src('client/css/sass/styles.scss')
        .pipe(sass())
        .pipe(gulp.dest('client/css/dist'));
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
    return gulp.src('client/js/dev/**/*.js')
        .pipe(concat('client/js/dev/all.js'))
        .pipe(gulp.dest('client/js/dist'))
        .pipe(rename('all.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('client/js/dist'));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('client/js/**/*.js', ['lint' /*, 'scripts'*/]);
    gulp.watch('client/css/sass/**/*.scss', ['sass']);
    gulp.watch('client/index.html', ['default']);
    livereload.listen();
});

// Default Task
gulp.task('default', ['lint', 'sass' /* , 'scripts'*/, 'watch']);
